<?php

namespace console\controllers;

use yii\console\Controller;


class ParseController extends Controller
{
    /**
     *
     * @param string $string
     * @param bool $isString
     * @return void
     */
    public function actionIndex(string $string, bool $isString = false)
    {
        if ($isString) {
            $stringContent = $string;
        } else {
            //получаем содержимое файла
            $stringContent = file_get_contents($string);
        }
        //убираем переносы строк
        $clearString = str_replace(["\r\n\r\n", "\n\n", "\r\r"], ' ', $stringContent);
        //убираем знаки препинания
        $clearString = preg_replace('/\pP/iu', '', $clearString);
        //создаем массив
        $wordsArr = explode(' ', $clearString);
        //формируем массив вида ['слово' => кол-во вхождений]
        $countArr = array_count_values($wordsArr);
        //сортируем массив
        array_multisort(array_values($countArr), SORT_DESC, array_keys($countArr), SORT_ASC, $countArr);

        print_r($countArr);
    }
}
